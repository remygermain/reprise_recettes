 <?php
error_reporting(E_ALL);

include('functions.php');
$mysqli = connect();

// Chargement des listes
$datasName = array('occasion', 'regime', 'sante', 'type_cuisson', 'type_plat');
$datas = array();
foreach( $datasName as $d ){
    $res = $mysqli->query("SELECT * FROM data_" . $d);
    while ($row = $res->fetch_assoc()) {
        $datas[$d][$row['id']] = $row['name'];
    }
}

$files = array( 'recettes_topsante.xml',
                'recettes_grazia.xml',
                'recettes_elleadore.xml');

foreach($files as $file){
    if (file_exists($file)) {
        try {
            $xml = simplexml_load_file($file);
        } catch (Exception $e) {
            echo 'Exception : ',  $e->getMessage(), "\n";
            $errors = libxml_get_errors();
            foreach ($errors as $error) {
                echo display_xml_error($error, $xml);
            }
            exit;
        }

        $total = count($xml->recettes->item);
        echo 'Nombre d\'éléments à traiter : ' . $total . PHP_EOL;

        foreach ($xml->recettes->item as $item){
            $new_item = array();
            $ingredients = array();
            $occasions = array();

            foreach( $item->children() as $child){
                $key = $child->getName();
                if ($key === 'ingredients'){
                    if ($child->count() > 0){
                        foreach( $child->children() as $ingredient){
                            $ingredients[] = array( 'ingredient' => (string)$ingredient->nom, 
                                                    'quantite' => (string)$ingredient->quantite, 
                                                    'unite' => (string)$ingredient->unite, );
                        }
                    }
                }
                elseif (in_array($key, $datasName)){
                    $value = (string)$child;
                    if($key == 'occasion'){
                        $values = array();
                        if(strpos($value, '#') !== false){
                            $values = explode('#', $value);
                        }
                        elseif( trim($value) !== ''){
                            $values = array($value);
                        }
                        foreach($values as $v){
                            if($v == 'Mardi Gras/Chandeleur') 
                                $v = 'Mardi Gras / Chandeleur';
                            elseif($v == 'Recettes pour bébé'){
                                $v = 'Recettes pour enfant';
                            }
                            elseif($v == 'Saint-Valentin'){
                                $v = 'Saint Valentin';
                            }
                            elseif($v == 'halloween'){
                                $v = 'Halloween';
                            }
                            elseif($v == 'brunch/petit déjeuner'){
                                $v = 'Brunch/Petit déjeuner';
                            }
                            elseif($v == 'réception/buffet'){
                                $v = 'Réception/Buffet';
                            }
                            elseif($v == 'plats de fête'){
                                $v = 'Plats de fête';
                            }
                            elseif($v == 'cocktail/apéritif'){
                                $v = 'Cocktail/Apéritif';
                            }

                            $new_val = array_search($v, $datas[$key]);
                            if($new_val === false){
                                echo PHP_EOL. 'Correspondance non trouvée : '. $v;
                            }
                            else{
                                $occasions[] = array_search($v, $datas[$key]);
                            }
                        }
                    }
                    else{
                        $key_found = array_search($value, $datas[$key]);
                        $new_item[$key] = $key_found;
                    }
                }
                elseif($key === 'auteur'){
                    if(isset($child->id)){
                        $new_item['auteur_id'] = (string)$child->id;
                    }
                    else{
                        $new_item[$key] = (string)$child;
                    }
                }
                else {
                    $new_item[$key] = (string)$child;
                }
            }

            addRecipe($mysqli, $new_item);

            $id_recette = $mysqli->insert_id;

            if ($id_recette !== 0){
                if(!empty($occasions)){
                    addOccasions($mysqli, $id_recette, $occasions);
                }

                if(!empty($ingredients)){
                    addIngredients($mysqli, $id_recette, $ingredients);
                }
            }

        }
        $mysqli->commit();
        
    } else {
        exit('Echec lors de l\'ouverture du fichier ' . $file);
    }
}
$mysqli->close();
?>