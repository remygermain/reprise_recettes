 <?php
error_reporting(E_ALL);

include('functions.php');

$mysqli = connect();

//$res  = $mysqli->query("SELECT * FROM ingredients WHERE quantite = '' ");
//$res  = $mysqli->query("SELECT * FROM ingredients WHERE id >= 81400 AND id < 81420");

// On crée la table
$res = $mysqli->query("CREATE TABLE IF NOT EXISTS ingredients_test LIKE ingredients");+
$res = $mysqli->query("TRUNCATE TABLE ingredients_test");
$res = $mysqli->query("INSERT INTO ingredients_test SELECT * FROM ingredients");



$res = $mysqli->query("SELECT * FROM ingredients");

while ($row = $res->fetch_assoc()) {

    if ($row['ingredient'] != '' && $row['quantite'] != '' && $row['unite'] != ''){ 
        continue; 
    }

    $quantite = $row['quantite'];
    $unite = $row['unite'];
    $ingredient = '';
    $string = $row['ingredient'];
//echo 'Traitement de : ' . $string . PHP_EOL;

    //preg_match('/(\d+)((\s|[A-z]).*)\s((d|\().*)$/Uis', $string, $matches);
    preg_match('/(\d+)((\s|[A-z]).*)\s((d\'|de ).*)$/Uis', $string, $matches);
//var_dump($matches);
    if (empty($matches)){
        // cas "12 moules"
        preg_match('/(\d+)\s(.*)$/Uis', $string, $matches);
//var_dump($matches);
        if (empty($matches)){
            // cas "50cl eau" ou "c à s de soja"
            preg_match('/(c(\s|\.)?(a|à)?(\s|\.)(c|café|cafe))\s(d\'|de )(.*)$/Uis', $string, $matches);
            //var_dump($matches);
            if(empty($matches)){
                preg_match('/(c(\s|\.)?(a|à)?(\s|\.)(s|soupe))\s(d\'|de )(.*)$/Uis', $string, $matches);
            }
            if(!empty($matches)){
                $unite = (isset($matches[1]))? trim($matches[1]) : '';
                $ingredient = (isset($matches[7]))? trim($matches[7]) : '';
            }
            else{
                preg_match('/(cl|g)+\s(de)?(.*)$/Uis', $string, $matches);
                if(!empty($matches)){
                    $unite = (isset($matches[1]))? trim($matches[1]) : '';
                    $ingredient = (isset($matches[3]))? trim($matches[3]) : '';
                }
            }
        }
        else{
            $quantite = isset($matches[1])? trim($matches[1]) : '';
            $ingredient = isset($matches[2])? trim($matches[2]) : '';
        }
    }
    else{
        $unite = isset($matches[2])? trim($matches[2]) : '';
        $quantite = isset($matches[1])? trim($matches[1]) : '';
        $ingredient = isset($matches[4])? trim($matches[4]) : '';
        preg_match('/de\s(.*)$/Uis', $ingredient, $parts);
//var_dump($parts);
        if (isset($parts[1]) && trim($parts[1]) !== ''){
            $ingredient = trim($parts[1]);
        }
        else{
            preg_match("/d'(.*)$/Uis", $ingredient, $parts);
            if (isset($parts[1]) && trim($parts[1]) !== ''){
                $ingredient = trim($parts[1]);
            }
        }
    }

    $tmp = array($quantite, $unite, $ingredient);
    //var_dump($tmp);
//echo 'Insertion de : ' . $ingredient . PHP_EOL;

    if($ingredient != '') {
        $sql = "UPDATE ingredients_test set ingredient='" . $mysqli->real_escape_string($ingredient) . "', quantite = '" . $mysqli->real_escape_string($quantite) . "', unite = '" .$mysqli->real_escape_string($unite) ."' WHERE id = " . $row['id'];
        $mysqli->query($sql);
    }
    else{
//var_dump($string);
    }
}

$mysqli->commit();
$mysqli->close();


?>