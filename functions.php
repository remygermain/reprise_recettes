<?

function connect(){
	$mysqli = new mysqli("localhost", "root", "pass", "recettes");
	return $mysqli;
}

function addRecipe($mysqli, $data){
	$columns = array_keys($data);
	foreach ($data as $key => $d){
		$data[$key] = $mysqli->real_escape_string($d);
	}
	$sql = "INSERT INTO recettes(".implode(',', $columns) .") values('".implode("','",$data)."')";
	$mysqli->query($sql);
}

function addOccasions($mysqli, $id_recette, $data ){
	foreach ($data as $id_occasion){
		$sql = 'INSERT INTO occasions(id_recette, id_occasion) values(' . $id_recette . ', ' . $id_occasion .')';
		$mysqli->query($sql);
	}
}

function addIngredients($mysqli, $id_recette, $data){
	foreach($data as $ing){
		foreach ($ing as $key => $d){
			$ing[$key] = $mysqli->real_escape_string($d);
		}
		$sql = 'INSERT INTO ingredients(id_recette, '.implode(', ', array_keys($ing)) . ') values( '. $id_recette. ', \''.implode("','",$ing).'\')';
//echo $sql;
		$mysqli->query($sql); 
	}
}